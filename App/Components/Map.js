import React, {
    Component
}
from 'react';

import {
    StyleSheet,
    Image,
    MapView
}
from 'react-native';

var styles = StyleSheet.create({
    mapview: {
        flex:1,
        flexDirection: 'column',
        justifyContent: 'center',
    }
});

class Map extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return ( 
            <MapView style={styles.mapview}
                    showsUserLocation={true}
                    annotations={this.props.markers}
                    region={this.props.region}
            />
        )
    }
};

module.exports = Map;