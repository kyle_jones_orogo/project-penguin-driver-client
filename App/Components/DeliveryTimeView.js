import React, {
    Component
}
from 'react';

import {
    StyleSheet,
    Text,
    View
}
from 'react-native';

var styles = StyleSheet.create({
    deliverytimeview: {
        height:100,
        backgroundColor: '#ffffff',
        borderBottomWidth:1,
        padding: 12
    },
    noticeText: {
        fontWeight: "100",
        fontSize:10
    },
    countdownText: {
        fontWeight: "500",
        fontSize:24,
        marginTop: 10
    }
});

class DeliveryTimeView extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return ( 
            <View style={styles.deliverytimeview}>
                <Text style={styles.noticeText}>Keep an eye out, your order is only</Text>
                <Text style={styles.countdownText}>{this.props.orderTime} mins away</Text>
            </View>
        )
    }
};

module.exports = DeliveryTimeView;



