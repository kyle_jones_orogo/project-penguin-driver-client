import * as firebase from 'firebase';

const firebaseRoot = "https://pigeonproject-9b18a.firebaseio.com",
    ordersRef = "/orders/",
    orderHistoryRef = "/orderHistory/";
// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyDo51YnIRa6Ppt4WaznJ43CBBWRCV5uzRQ",
    authDomain: "pigeonproject-9b18a.firebaseapp.com",
    databaseURL: firebaseRoot,
    storageBucket: "pigeonproject-9b18a.appspot.com",
};
const firebaseApp = firebase.initializeApp(firebaseConfig);
var database = firebase.database();

class GeoService {

    constructor(orderID, updateDriverLocation, updateCustomerLocation, updateETA) {

        this.pollInterval = 5000;
        this.orderID = orderID;

        this.updateDriverLocation = updateDriverLocation;
        this.updateCustomerLocation = updateCustomerLocation;
        this.updateETA = updateETA;

        this.customerLocation = {
            latitude: 0,
            longitude: 0           
        };
        this.driverPosition = {
            latitude: 0,
            longitude: 0
        };
        var self = this;

        watchOptions = {
            timeout : 60*1000,
            maxAge: 1000,
            enableHighAccuracy: true
        };

        //Check for any change in GPS location and update firebase if so
        this.watchID = navigator.geolocation.watchPosition((position) => {
            this.updateDriverLocation(position);
            this.driverPosition = position.coords;
            this.sendDriverLocation(position);
            this.calculateDistance();
        }, (error) => {
            //alert('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
        }, watchOptions);

        //Watch on firebase for changes on the customer
        firebase.database().ref(ordersRef + this.orderID + '/client').on('value', function(snapshot) {
            let loc = snapshot.val();
            let customerLocation = {
                latitude: loc.latitude,
                longitude: loc.longitude,
                image: require('../Img/icon.png')
            };
            self.customerLocation = customerLocation;
            self.updateCustomerLocation([self.customerLocation]);
            self.calculateDistance();
        });
    }

    tearDown() {
        navigator.geolocation.clearWatch(this.watchID);
    }

    //TODO: Kyle - This should be refactored into a firebase service
    sendDriverLocation(location, callback) {
        try {
            if (location.coords) {
                //Current location
                firebase.database().ref(ordersRef + this.orderID + '/driver').set({
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude
                });
                //Location history
                let now = new Date();
                firebase.database().ref(orderHistoryRef + this.orderID + '/driver').push({
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude,
                    timestamp: now.toUTCString()
                });
                this.calculateDistance();
            }
        } catch (error) {
            console.error(error);
        }
    }

    calculateDistance(callback) {
        function toRad(degrees) {
            return degrees * Math.PI / 180;
        }

        var R = 6371e3; // metres
        var φ1 = toRad(this.customerLocation.latitude);
        var φ2 = toRad(this.driverPosition.latitude);
        var Δφ = toRad(this.driverPosition.latitude-this.customerLocation.latitude);
        var Δλ = toRad(this.driverPosition.longitude-this.customerLocation.longitude);

        var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
                Math.cos(φ1) * Math.cos(φ2) *
                Math.sin(Δλ/2) * Math.sin(Δλ/2);

        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        var d = R * c;
        
        var minutes = Math.round(d / (20000 / 60));

        //Assuming 20kph
        this.updateETA(minutes);
    }
}

module.exports = GeoService;