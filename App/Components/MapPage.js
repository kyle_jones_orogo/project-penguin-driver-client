import React, {
    Component
}
from 'react';

import {
    StyleSheet,
    Text,
    View
}
from 'react-native';

import * as firebase from 'firebase';

var DeliveryTimeView = require('./DeliveryTimeView.js');
var Map = require('./Map.js');
var GeoService = require('./GeoService.js');

var styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#EEEEEE',
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5,
        marginTop: 70
    },
    innerContainer: {
        flex: 1,
        borderWidth: 1,
    }
});

class MapPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            orderID: 2,
            orderTime: 0,
            region: {
                latitude: 51.45,
                longitude: -2.58,
                latitudeDelta: 0.01,
                longitudeDelta: 0.01
            },
            markers: [{
                latitude: 0,
                longitude: 0,
                image: require('../Img/icon.png')
            }],
            position: { 
                coords: {
                    latitude: 0,
                    longitude: 0
                }
            }
        };
        this.updateDriverLocation = this.updateDriverLocation.bind(this);
        this.updateCustomerLocation = this.updateCustomerLocation.bind(this);
        this.updateETA = this.updateETA.bind(this);
    }

    updateDriverLocation(position) {
        this.setState({
            position: position
        })
    }

    updateCustomerLocation(marker) {
        this.setState({
            markers: marker
        })
    }

    updateETA(minutes) {
        this.setState({
            orderTime: minutes
        })
    }

    render() {
        return (<View style={styles.mainContainer}>
                    <View style={styles.innerContainer}>
                        <DeliveryTimeView orderTime={this.state.orderTime} />
                        <Map markers={this.state.markers} region={this.state.region}/>
                    </View>
                </View>         
        )
    }

    componentDidMount() {
        var geoService = new GeoService(this.state.orderID, this.updateDriverLocation, this.updateCustomerLocation, this.updateETA);
        this.setState({
            geoService: geoService
        });
    }

    componentWillUnmount() {
        this.geoService.tearDown();
    }
}

module.exports = MapPage;